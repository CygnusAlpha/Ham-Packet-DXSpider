#!/usr/bin/perl
# Test Ham::Packet::DXSpider

# TODO: Need to properly test the module using the mock.

use IO::Socket::INET;
use Test::More( tests => 5 );

use_ok( 'Ham::Packet::DXSpider' );

# Mock the telnet connection tp DXSpider
my $mock_sock = IO::Handle->new();

$dxs=new Ham::Packet::DXSpider( callsign => 'test_callsign', handle => $mock_sock );
ok( defined( $dxs ), 'new( callsign, socket handle )' );

ok( $dxs->addStatsHandler(          \&Ham::Packet::DXSpider::defaultStatsHandler ),		        'addStatsHandler()' );
ok( $dxs->addDXMessageHandler(      \&Ham::Packet::DXSpider::defaultDXMessageHandler ),	        'addDXMessageHandler()' );
ok( $dxs->addPrivateMessageHandler( \&Ham::Packet::DXSpider::defaultPrivateMessageHandler ),	'addPrivateMessageHandler()' );

#ok( $dxs->sendPrivate( 'test_callsign', 'test message',"This\nis\na\ntest" ),	'sendPrivate()' );

#$dxs->start();


